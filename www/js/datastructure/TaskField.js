// This class defines a field in a task, for use by other classes to search and sort by these fields.
function TaskField(name, prettyName, type, canBeNull, getFromTask) {
  // The internal name of the field.
  this.name = name;

  // The user-readable name of the field.
  this.prettyName = prettyName;

  // The data type of the field.
  this.type = type;

  // A field that indicates if this field can be null.
  this.canBeNull = canBeNull;

  // The function that gets the field from the task.
  this.getFromTask = getFromTask;
}

// An enum containing all known task fields.
var allTaskFields = {
  // Blank placeholder value.
  blank: new TaskField(
    "blank",
    "",
    "",
    false,
    function() {
      return null;
    }
  ),

  // The title of the task.
  title: new TaskField(
    "title",
    "title",
    "string",
    false,
    function(task) {
      return task.title;
    }
  ),

  // Whether or not the task is completed.
  completed: new TaskField(
    "completed",
    "completed",
    "boolean",
    false,
    function(task) {
      return task.completed;
    }
  ),

  // The creation date of the task.
  creationDate: new TaskField(
    "creationDate",
    "creation date",
    "Date",
    false,
    function(task) {
      return task.creationDate;
    }
  ),

  // The most recent edit date of the task.
  editDate: new TaskField(
    "editDate",
    "last edit date",
    "Date",
    false,
    function(task) {
      return task.editDate;
    }
  ),

  // The due date of the task.
  dueDate: new TaskField(
    "dueDate",
    "due date",
    "Date",
    true,
    function(task) {
      return task.dueDate;
    }
  ),

  // The completion date of the task.
  completionDate: new TaskField(
    "completionDate",
    "completion date",
    "Date",
    true,
    function(task) {
      return task.completionDate;
    }
  ),

  // The subtasks.
  subtasks: new TaskField(
    "subtasks",
    "subtasks",
    "Array",
    false,
    function(task) {
      return task.subtasks;
    }
  ),

  // The number of subtasks.
  subtasksLength: new TaskField(
    "subtasksLength",
    "number of subtasks",
    "number",
    false,
    function(task) {
      return task.subtasks.length;
    }
  ),

  // The tags.
  tags: new TaskField(
    "tags",
    "tags",
    "Array",
    false,
    function(task) {
      return task.tags;
    }
  ),

  // The number of tags.
  tagsLength: new TaskField(
    "tagsLength",
    "number of tags",
    "number",
    false,
    function(task) {
      return task.tags.length;
    }
  )
};
