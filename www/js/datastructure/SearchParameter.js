// This class defines a requirement for a task to be included in a view.
function SearchParameter(field, comparator, value) {
  // The field that the parameter operates on.
  this.field = field;

  // The comparator this parameter uses.
  this.comparator = comparator;

  // The value this parameter uses.
  this.value = value;

  // Get this parameter's description in plain English.
  this.description = function () {
    return this.comparator.prettyPrint(
      this.field.prettyName,
      prettyValue(this.value)
    );
  };

  // Print the value of the parameter in plain English.
  var prettyValue = function (value) {
    // Format Date objects.
    if (value instanceof Date) {
      return value.toDateString();
    }

    // Format RelativeDate objects.
    if (value instanceof RelativeDate) {
      return value.prettyPrint();
    }

    // Return anything else verbatim.
    return value;
  };

  // Determine if this parameter allows the given task.
  this.matches = function (task) {
    var taskValue = this.field.getFromTask(task);

    return this.comparator.compare(taskValue, this.evaluateValue());
  };

  // Get the date type.
  this.getDateType = function () {
    // If the value is a RelativeDate, return the type of RelativeDate.
    if (this.value instanceof RelativeDate) {
      return this.value.name;
    }

    // If the value is a Date, return the string "specificDate".
    if (this.value instanceof Date) {
      return "specificDate";
    }

    // If the value isn't a date, return null.
    return null;
  };

  // Update this parameter using the information in a viewDetail object.
  this.createFromViewDetail = function (viewDetail) {

    // Set the field and comparator
    this.field = allTaskFields[viewDetail.field];
    this.comparator = allParameterComparators[viewDetail.comparator];

    // Set the appropriate value for the field.
    switch (this.comparator.canonicalValueType()) {
      // Set the value from the numberValue field if it's a number.
      case "number":
        this.value = parseInt(viewDetail.stringValue);
        return;

      // Set the value from the stringValue field if it's a string.
      case "string":
        this.value = viewDetail.stringValue;
        return;

      case "Date":
        if (viewDetail.dateType === "specificDate") {
          // Set the value from the dateValue field if it's a specific date.
          this.value = viewDetail.dateValue;
        } else {
          // Create a RelativeDate as the value from the dateType field using the numberValue field as the offset.
          this.value = new RelativeDate(viewDetail.dateType, parseInt(viewDetail.stringValue));
        }
        return;

      case "null":
        return;

      default:
        console.log("Warning: Unknown value type: " + this.comparator.canonicalValueType());
        return;
    }
  };

  // Evaluate the value.
  this.evaluateValue = function () {
    // Compute a Relative Date as a real date.
    if (this.value instanceof RelativeDate) {
      return this.value.evaluateDate();
    }

    // For all other values, simply return the value.
    return this.value;
  };

  // Determine if this parameter is valid.
  this.isValid = function () {
    // Make sure the field name is valid.
    if (!this.field || !allTaskFields.hasOwnProperty(this.field.name)) {
      console.log("Field not valid: " + angular.toJson(this.field));
      return false;
    }

    // Make sure the comparator is valid.
    if (!this.comparator || !allParameterComparators.hasOwnProperty(this.comparator.name) ||
        this.comparator.allowedFieldType !== this.field.type) {

      console.log("Comparator not valid: " + angular.toJson(this.comparator));
      return false;
    }

    // Make sure the value is valid.
    if (!this.comparator.isApplicableToValue(this.value)) {
      console.log("Comparator not applicable to value: " + angular.toJson(this.comparator) + angular.toJson(this.value));
      return false;
    }

    // If the value is a number, make sure it's not NaN. Set it to 0 instead.
    if(typeof this.value === "number") {
      if(isNaN(this.value)) {
        this.value = 0;
      }
    }

    // If the value is a RelativeDate object, validate it.
    if (this.value instanceof RelativeDate) {
      return this.value.isValid();
    }

    // Return true.
    return true;
  };

  // Get the value of this parameter as a String, if applicable.
  this.getStringValue = function () {
    // If the value is a string, return it.
    if (typeof this.value === "string") {
      return this.value;
    }

    // If the value is a number, return it as a string.
    if (typeof this.value === "number") {
      return "" + this.value;
    }

    // If the value is a relative date, return the offset.
    if(this.value instanceof RelativeDate) {
      return "" + this.value.offset;
    }

    // If the value is of some other type or doesn't exist, return the empty string.
    return "";
  };

  // Get the value of this parameter as a date, if applicable.
  this.getDateValue = function () {
    // If the value is a simple date, just return it.
    if (this.value instanceof Date) {
      return this.value;
    }

    // If the value is a relative date, evaluate it.
    if (this.value instanceof RelativeDate) {
      return this.value.evaluateDate();
    }

    // Default to now.
    return new Date();
  };

  // Turn this search parameter into a copy of the given object.
  // Intended for turning JSON deserialized objects into functional ones.
  this.copyFrom = function(obj) {
    this.field = allTaskFields[obj.field.name];
    this.comparator = allParameterComparators[obj.comparator.name];

    if(this.comparator.canonicalValueType() === "Date") {
      // If the value type is a date or RelativeDate, it needs to be converted.
      if(typeof obj.value === "string") {
        // If the value is a string, that represents a concrete Date. Parse it using the Date constructor.
        this.value = new Date(obj.value);
      } else {
        // Else, it's a relative date.
        this.value = new RelativeDate("", 0);
        this.value.copyFrom(obj.value);
      }
    } else {
      // Any other value is a simple data type that can be copied over directly.
      this.value = obj.value;
    }
  }
}
