// A class that represents a relative date.
function RelativeDate(name, offset) {
  // The name of this relative date type.
  this.name = name;

  // The offset to apply to the starting date, in days.
  this.offset = parseInt(offset);

  // A function that prints this date in a user-readable manner.
  this.prettyPrint = function () {
    return prettyPrintFunctions[this.name](this.offset);
  };

  // A function that evaluates this date relative to the current date.
  this.evaluateDate = function () {
    var result = getDateFunctions[this.name]();
    result.setDate(result.getDate() + this.offset);
    return result;
  };

  // A function that validates this RelativeDate and makes sure it makes sense.
  this.isValid = function() {
    // Make sure this RelativeDate's name exists.
    if(!getDateFunctions.hasOwnProperty(this.name)) {
      return false;
    }

    // Make sure the offset is a valid integer. Set it to 0 if it's not a number.
    if(isNaN(this.offset)) {
      this.offset = 0;
    }

    // Round it down if it's not an integer.
    this.offset = Math.floor(this.offset);

    return true;
  };

  // Turn this relative date into a copy of the given object.
  // Intended for turning JSON deserialized objects into functional ones.
  this.copyFrom = function(obj) {
    this.name = obj.name;
    this.offset = obj.offset;
  }
}

// An enum containing the names of all known RelativeDate objects.
var allRelativeDateNames = {
  relativeDay: "relativeDay",
  relativeWeek: "relativeWeek",
  relativeMonth: "relativeMonth",
  relativeYear: "relativeYear"
};

// An enum containing all known getDateFunctions for RelativeDates.
var getDateFunctions = {

  // A function that returns today at midnight (midnight last night)
  relativeDay: function () {
    var result = new Date();
    result.setHours(0, 0, 0, 0);
    return result;
  },

  // A function that returns Sunday of this week at midnight.
  relativeWeek: function () {
    var result = new Date();
    result.setHours(0, 0, 0, 0);

    // Subtract days to get to Sunday of this week.
    // The JS Date object handles the case where this wraps to the previous month or year.
    var dayOfWeek = result.getDay();
    var dayOfMonth = result.getDate();
    result.setDate(dayOfMonth - dayOfWeek);

    return result;
  },

  // A function that returns the first of this month at midnight.
  relativeMonth: function () {
    var result = new Date();
    result.setDate(1);
    result.setHours(0, 0, 0, 0);
    return result;
  },

  // A function that returns the first of this month at midnight.
  relativeYear: function () {
    var result = new Date();
    result.setMonth(0, 1);
    result.setHours(0, 0, 0, 0);
    return result;
  }
};

// An enum containing all known pretty print functions for RelativeDates.
var prettyPrintFunctions = {

  // Pretty print dates relative to today.
  relativeDay: function (offset) {
    switch (offset) {
      case 0:
        return "today";

      case 1:
        return "tomorrow";

      case -1:
        return "yesterday";

      default:
        if (offset > 0) {
          return offset + " days from now";
        } else {
          return (-offset) + " days ago";
        }
    }
  },

  // Pretty print dates relative to Sunday this week.
  relativeWeek: function (offset) {
    // Find the week by dividing by 7.
    var week = Math.floor(offset / 7);

    // Find the day of the week by taking the modulus of the offset.
    var dayOfWeek = offset % 7;

    if (dayOfWeek < 0) {
      dayOfWeek += 7;
    }

    var result = "";

    // Append the day of the week to the result value.
    switch (dayOfWeek) {
      case 0:
        result += "Sunday";
        break;

      case 1:
        result += "Monday";
        break;

      case 2:
        result += "Tuesday";
        break;

      case 3:
        result += "Wednesday";
        break;

      case 4:
        result += "Thursday";
        break;

      case 5:
        result += "Friday";
        break;

      case 6:
        result += "Saturday";
        break;

      default:
        result += "ERROR";
        console.log("Warning: undefined day of week: " + dayOfWeek);
        break;
    }

    // Append the week to the result value.
    switch (week) {
      case 0:
        result += " this week";
        break;

      case 1:
        result += " next week";
        break;

      case -1:
        result += " last week";
        break;

      default:
        if (week > 0) {
          result += " " + week + " weeks from now";
        } else {
          result += " " + (-week) + " weeks ago";
        }
        break;
    }

    return result;
  },

  // Pretty print dates relative to the first of this month.
  relativeMonth: function (offset) {
    if (offset === 0) {
      return "the 1st of this month";
    } else if (offset > 0) {
      return offset + " days after the 1st of this month";
    } else {
      return (-offset) + " days before the 1st of this month";
    }
  },

  // Pretty print dates relative to the first of this year.
  relativeYear: function (offset) {
    if (offset === 0) {
      return "Jan 1st this year";
    } else if (offset > 0) {
      return offset + " days after Jan 1st this year";
    } else {
      return (-offset) + " days before Jan 1st this year";
    }
  }
};
