// This class defines a comparator used by a search parameter.
function ParameterComparator(name, prettyPrint, compare, allowedFieldType, allowedValueTypes) {
  // The name of the comparator.
  this.name = name;

  // A function that takes the name of the field and the value
  // and prints the behavior of the comparator in a user-readable way.
  this.prettyPrint = prettyPrint;

  // The comparison function.
  // Takes the task's value and the specified value and performs the comparison,
  // returning true if the value meets the requirements.
  this.compare = compare;

  // The field this comparator is applicable to.
  this.allowedFieldType = allowedFieldType;

  // The types allowed for this comparator.
  this.allowedValueTypes = allowedValueTypes;

  // Get the canonical value this comparator operates on.
  // This is the first item in the allowedValueTypes array, if it exists.
  // If it doesn't exist, return null.
  this.canonicalValueType = function () {
    if(this.allowedValueTypes.length === 0) {
      return null;
    }

    return this.allowedValueTypes[0];
  };

  // Determine if this comparator is applicable to the given value.
  this.isApplicableToValue = function (value) {
    // If the allowed types array is empty,
    // that means this comparator doesn't take a comparison value, so return true.
    if (this.allowedValueTypes.length === 0) {
      return true;
    }

    var result = false;

    // Check each allowed value type one by one.
    this.allowedValueTypes.forEach(function(type) {
      if (valueIsOfType(type, value)) {
        result = true;
      }
    });

    return result;
  };

  // Determine if the given value is a member of the type represented by the string type.
  var valueIsOfType = function (type, value) {

    // This check catches the basic data types boolean, string, and number.
    if (typeof value === type) {
      return true;

    } else if (typeof value === "object") {
      // This check handles advanced datatypes ("classes").
      // To the best of my knowledge, this is the best way to do this in Javascript.
      // If this isn't the case I'd love to know.
      switch (type) {
        case "Date":
          return value instanceof Date;
        case "RelativeDate":
          return value instanceof RelativeDate;
        case "Array":
          return value instanceof Array;
        default:
          console.log("Warning: Unknown type: " + type);
      }

      return false;
    }

    return false;
  }
}

// Utility function used by several date comparators.
// Returns a new date that represents the given date rounded back to midnight.
function roundBackToMidnight(date) {
  var midnight = new Date(date.getTime());
  midnight.setHours(0, 0, 0, 0);
  return midnight;
}

// An enum containing all known parameter comparators.
var allParameterComparators = {
  // Blank placeholder comparator.
  // Applicable for no values. Allows all tasks.
  blank: new ParameterComparator(
    "blank",
    function () {
      return "";
    },
    function () {
      return true;
    },
    [],
    []
  ),

  // The date not-null comparator.
  // Requires the date be not null.
  isSet: new ParameterComparator(
    "isSet",
    function (x) {
      return "The task has a " + x;
    },
    function (x) {
      return x !== null;
    },
    "Date",
    []
  ),

  // The date null comparator.
  // Requires the date be not null.
  isNotSet: new ParameterComparator(
    "isNotSet",
    function (x) {
      return "The task does not have a " + x;
    },
    function (x) {
      return x === null;
    },
    "Date",
    []
  ),

  // The date equals comparator.
  // Requires the date be on the specified date or null.
  on: new ParameterComparator(
    "on",
    function (x, y) {
      return "The " + x + " is on " + y;
    },
    function (x, y) {
      return x === null || roundBackToMidnight(x) === y;
    },
    "Date",
    ["Date", "RelativeDate"]
  ),

  // The date not-equals comparator.
  // Requires the date not be on the specified date or null.
  notOn: new ParameterComparator(
    "notOn",
    function (x, y) {
      return "The " + x + " is not on " + y;
    },
    function (x, y) {
      return x === null || roundBackToMidnight(x) !== y;
    },
    "Date",
    ["Date", "RelativeDate"]
  ),

  // The date greater-than comparator.
  // Requires the date be after the specified date or null.
  after: new ParameterComparator(
    "after",
    function (x, y) {
      return "The " + x + " is after " + y;
    },
    function (x, y) {
      return x === null || roundBackToMidnight(x) > y;
    },
    "Date",
    ["Date", "RelativeDate"]
  ),

  // The date greater-than-or-equal-to comparator.
  // Requires the date be on or after the specified date, or null.
  onAfter: new ParameterComparator(
    "onAfter",
    function (x, y) {
      return "The " + x + " is on or after " + y;
    },
    function (x, y) {
      return x === null || x >= y;
    },
    "Date",
    ["Date", "RelativeDate"]
  ),

  // The date less-than comparator.
  // Requires the date be before the specified date or null.
  before: new ParameterComparator(
    "before",
    function (x, y) {
      return "The " + x + " is before " + y;
    },
    function (x, y) {
      return x === null || x < y;
    },
    "Date",
    ["Date", "RelativeDate"]
  ),

  // The date less-than-or-equal-to comparator.
  // Requires the date be on or before the specified date, or null.
  onBefore: new ParameterComparator(
    "onBefore",
    function (x, y) {
      return "The " + x + " is on or before " + y;
    },
    function (x, y) {
      return x === null || roundBackToMidnight(x) <= y;
    },
    "Date",
    ["Date", "RelativeDate"]
  ),

  // The boolean true comparator.
  // Requires that the task's value is true.
  isTrue: new ParameterComparator(
    "isTrue",
    function (x) {
      return "The task is " + x;
    },
    function (x) {
      return x;
    },
    "boolean",
    []
  ),

  // The boolean false comparator.
  // Requires that the task's value is false.
  isFalse: new ParameterComparator(
    "isFalse",
    function (x) {
      return "The task is not " + x;
    },
    function (x) {
      return !x;
    },
    "boolean",
    []
  ),

  // The string equals comparator.
  // Requires that the task's value equals the specified string.
  stringEquals: new ParameterComparator(
    "stringEquals",
    function(x, y) {
      return "The task's " + x + ' is "' + y + '\"';
    },
    function(x, y) {
      return x === y;
    },
    "string",
    ["string"]
  ),

  // The string not-equals comparator.
  // Requires that the task's value does not equal the specified string.
  stringNotEquals: new ParameterComparator(
    "stringNotEquals",
    function(x, y) {
      return "The task's " + x + ' is not "' + y + '\"';
    },
    function(x, y) {
      return x !== y;
    },
    "string",
    ["string"]
  ),

  // The string contains substring comparator.
  // Requires that the task's value contains the specified string.
  stringContains: new ParameterComparator(
    "stringContains",
    function(x, y) {
      return "The task's " + x + ' has the substring "' + y + '\"';
    },
    function(x, y) {
      return x.indexOf(y) >= 0;
    },
    "string",
    ["string"]
  ),

  // The string not-contains substring comparator.
  // Requires that the task's value does not contain the specified string.
  stringNotContains: new ParameterComparator(
    "stringContains",
    function(x, y) {
      return "The task's " + x + ' does not have the substring "' + y + '\"';
    },
    function(x, y) {
      return x.indexOf(y) < 0;
    },
    "string",
    ["string"]
  ),

  // The array includes comparator.
  // Requires that the array includes the specified string.
  arrayIncludes: new ParameterComparator(
    "arrayIncludes",
    function (x, y) {
      return "The " + x + ' include "' + y + '"';
    },
    function (x, y) {
      return x.indexOf(y) >= 0;
    },
    "Array",
    ["string"]
  ),

  // The array does-not-include comparator.
  // Requires that the task's value does not include the specified string.
  arrayNotIncludes: new ParameterComparator(
    "arrayNotIncludes",
    function (x, y) {
      return "The " + x + ' do not include "' + y + '"';
    },
    function (x, y) {
      return x.indexOf(y) < 0;
    },
    "Array",
    ["string"]
  ),

  // The array length equals comparator.
  // Requires the array's length be equal to the specified length.
  arrayLengthEquals: new ParameterComparator(
    "arrayLengthEquals",
    function (x, y) {
      return "The number of " + x + " is " + y;
    },
    function (x, y) {
      return x.length === y;
    },
    "Array",
    ["number"]
  ),

  // The array length not-equals comparator.
  // Requires the array's length not be equal to the specified length.
  arrayLengthNotEquals: new ParameterComparator(
    "arrayLengthNotEquals",
    function (x, y) {
      return "The number of  " + x + " is not " + y;
    },
    function (x, y) {
      return x.length !== y;
    },
    "Array",
    ["number"]
  ),

  // The array length greater-than comparator.
  // Requires the array's length be greater than the specified length.
  arrayLengthGreaterThan: new ParameterComparator(
    "arrayLengthGreaterThan",
    function (x, y) {
      return "The number of  " + x + " is greater than " + y;
    },
    function (x, y) {
      return x.length > y;
    },
    "Array",
    ["number"]
  ),

  // The array length greater-than-or-equal-to comparator.
  // Requires the array's length be greater than or equal to the specified length.
  arrayLengthGreaterThanEqual: new ParameterComparator(
    "arrayLengthGreaterThanEqual",
    function (x, y) {
      return "The number of  " + x + " is greater than or equal to " + y;
    },
    function (x, y) {
      return x.length >= y;
    },
    "Array",
    ["number"]
  ),

  // The array length less-than comparator.
  // Requires the array's length be less than the specified length.
  arrayLengthLessThan: new ParameterComparator(
    "arrayLengthLessThan",
    function (x, y) {
      return "The number of  " + x + " is less than " + y;
    },
    function (x, y) {
      return x.length < y;
    },
    "Array",
    ["number"]
  ),

  // The array length less-than-or-equal-to comparator.
  // Requires the array's length be less than or equal to the specified length.
  arrayLengthLessThanEqual: new ParameterComparator(
    "arrayLengthLessThanEqual",
    function (x, y) {
      return "The number of " + x + " is less than or equal to " + y;
    },
    function (x, y) {
      return x.length <= y;
    },
    "Array",
    ["number"]
  )
};
