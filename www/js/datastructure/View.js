// The View class defines a user-customizable set of parameters for viewing a subset of all tasks.
// This class determines which tasks are included in the view, and the way they are sorted.
function View(title) {
  // The name of the view.
  this.title = title;

  // The array of searching parameters.
  this.searchParameters = [];

  // The array of sorting parameters.
  this.sortParameters = [];

  // For a given set of tasks, return an array representing this view on those tasks.
  this.getTaskArray = function(allTasks) {
    var visibleTasks = [];

    // First, get the list of tasks visible in this view.
    for(var taskId in allTasks) {
      if(!allTasks.hasOwnProperty(taskId)) {
        continue;
      }

      var task = allTasks[taskId];
      if(this.sees(task)) {
        visibleTasks.push(task);
      }
    }

    // Next, sort the array.
    visibleTasks.sort(createSortFunction(this.sortParameters));

    return visibleTasks;
  };

  // Determine if the given task is visible in this view.
  this.sees = function (task) {
    for(var i in this.searchParameters) {
      if(!this.searchParameters.hasOwnProperty(i)) {
        continue;
      }
      var parameter = this.searchParameters[i];
      if(!parameter.matches(task)) {
        return false;
      }
    }
    return true;
  };

  // Create a sort function from an array of sorting parameters.
  var createSortFunction = function(sortParameters) {
    return function(a, b) {
      for(var i in sortParameters) {

        if(!sortParameters.hasOwnProperty(i)) {
          continue;
        }

        var parameter = sortParameters[i];
        var result = parameter.compare(a, b);

        // If the comparison returns anything other than zero, return that value.
        // Otherwise, proceed with the next parameter.
        if(result != 0) {
          return result;
        }
      }

      // If every parameter returned zero, return zero.
      return 0;
    }
  };

  // Turn this view into a copy of the given object.
  // Intended for turning JSON deserialized objects into functional ones.
  this.copyFrom = function(obj) {
    this.title = obj.title;

    // Copy the search parameters.
    this.searchParameters.splice(0, this.searchParameters.length);
    for(var i in obj.searchParameters) {
      if(!obj.searchParameters.hasOwnProperty(i)) {
        continue;
      }

      var objParameter = obj.searchParameters[i];
      var newParameter = new SearchParameter(null, null, null);
      newParameter.copyFrom(objParameter);
      this.searchParameters.push(newParameter);
    }

    // Copy the sort parameters.
    this.sortParameters.splice(0, this.sortParameters.length);
    for(i in obj.sortParameters) {
      if(!obj.sortParameters.hasOwnProperty(i)) {
        continue;
      }

      objParameter = obj.sortParameters[i];
      newParameter = new SortParameter(null, null, null);
      newParameter.copyFrom(objParameter);
      this.sortParameters.push(newParameter);
    }
  }
}
