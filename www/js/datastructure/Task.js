// This class defines a task that the user wants to record in the task manager.
function Task(title, taskId) {
  // The name of the task.
  this.title = title;

  // The unique id of the task.
  this.taskId = taskId;

  // Whether the task has been completed.
  this.completed = false;

  // The creation date.
  this.creationDate = new Date();

  // The completion date.
  this.completionDate = null;

  // The most recent edit date.
  this.editDate = new Date();

  // The due date.
  this.dueDate = null;

  // The array of subtasks.
  this.subtasks = [];

  // The array of tags.
  this.tags = [];

  // Toggle whether or not this task is completed.
  this.toggleCompleted = function() {
    this.completed = !this.completed;

    if(this.completed) {
      this.completionDate = new Date();
    } else {
      this.completionDate = null;
    }
  };

  // Turn this task into a copy of the given object.
  // Intended for turning JSON deserialized objects into functional ones.
  this.copyFrom = function(obj) {
    this.title = obj.title;
    // Skip taskId. There's no need to preserve that across sessions.
    this.completed = obj.completed;
    this.creationDate = copyDate(obj.creationDate);
    this.completionDate = copyDate(obj.completionDate);
    this.editDate = copyDate(obj.editDate);
    this.dueDate = copyDate(obj.dueDate);
    this.subtasks = obj.subtasks;
    this.tags = obj.tags;
  };

  // Create a date from a date string, or null if it is null.
  var copyDate = function(obj) {
    if(!obj) {
      return null;
    } else {
      return new Date(obj);
    }
  }
}
