// This class determines the order in which tasks should be sorted in a view.
function SortParameter(field, nullFirst, descending) {
  // The field that the parameter operates on.
  this.field = field;

  // If true, null values go first; if false, null values go last.
  this.nullFirst = nullFirst;

  // False for ascending, true for descending.
  this.descending = descending;

  // Get this parameter's description in plain English.
  this.description = function () {
    var result = "By " + this.field.prettyName + " ";
    result += this.descending ? "descending" : "ascending";

    if(this.field.canBeNull) {
      if(this.nullFirst) {
        result += ", blanks first";
      } else {
        result += ", blanks last";
      }
    }

    return result;
  };

  // Compare two tasks and indicate which comes first.
  // Return -1 for A first, 0 for tie, 1 for B first.
  this.compare = function (taskA, taskB) {
    // Get the values of the field for both tasks.
    var valueA = this.field.getFromTask(taskA);
    var valueB = this.field.getFromTask(taskB);

    // If the two are tied, return 0. (This handles the case where both are null.)
    if(valueA === valueB) {
      return 0;
    }

    // If exactly one of the values is null, sort appropriately.
    if((valueA === null && this.nullFirst) || (valueB === null && !this.nullFirst)) {
      return -1;
    } else if((valueA === null && !this.nullFirst) || (valueB === null && this.nullFirst)) {
      return 1;
    }

    // Otherwise, perform the comparison.
    var result = valueA < valueB ? -1 : 1;

    // Flip if descending.
    if(this.descending) {
      result = -result;
    }

    return result;
  };

  // Turn this sort parameter into a copy of the given object.
  // Intended for turning JSON deserialized objects into functional ones.
  this.copyFrom = function(obj) {
    this.field = allTaskFields[obj.field.name];
    this.nullFirst = obj.nullFirst;
    this.descending = obj.descending;
  }
}
