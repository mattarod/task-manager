var app = angular.module('taskManager',
  ['ionic', 'taskManager.features.controller', 'taskManager.features.service']);

// Set the configuration.
app.config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('main', {
      url: "/",
      templateUrl: "templates/main.html",
      controller: "MainController"
    })
    .state('taskDetail', {
      url: "/taskDetail/:taskId",
      templateUrl: "templates/taskDetail.html",
      controller: "TaskDetailController"
    })
    .state('views', {
      url: "/views/",
      templateUrl: "templates/views.html",
      controller: "ViewsController"
    })
    .state('viewDetail', {
      url: "/viewDetail/:viewId",
      templateUrl: "templates/viewDetail.html",
      controller: "ViewDetailController"
    });

  $urlRouterProvider.otherwise("/");
});

// Run the app.
app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});
