var module = angular.module('taskManager.features.service');

// The service that manages views.
module.factory('ViewService', function () {
  // Create a new view with a given title and add it to the allViews object.
  var addNewView = function (title) {
    var view = new View(title);
    allViews[view.viewId] = view;
    return view;
  };

  // Get the ID for a given view.
  var getIndex = function (view) {
    for (var i in allViews) {
      if (!allViews.hasOwnProperty(i)) {
        continue;
      }

      var currentView = allViews[i];
      if (currentView == view) {
        return i;
      }
    }

    // View doesn't exist, apparently.
    return null;
  };

  // Save all views to local storage.
  var save = function () {
    window.localStorage['views'] = angular.toJson(allViews);
  };

  // Reset to the default views.
  var resetToDefault = function () {
    // Delete all existing views.
    allViews.splice(0, allViews.length);

    // Shows all incomplete tasks, sorted by due date ascending (earliest first), blanks at top of list.
    // In case of ties, sort by last edit date descending (latest first.)
    var allTasksView = new View("All");
    allTasksView.searchParameters = [
      new SearchParameter(allTaskFields.completed, allParameterComparators.isFalse, null)
    ];
    allTasksView.sortParameters = [
      new SortParameter(allTaskFields.dueDate, true, false),
      new SortParameter(allTaskFields.editDate, false, true)
    ];
    allViews.push(allTasksView);

    // Shows all incomplete tasks due today or overdue, sorted by due date ascending (earliest first), blanks at bottom.
    // In case of ties, sort by last edit date descending (latest first).
    var todayView = new View("Today");
    todayView.searchParameters = [
      new SearchParameter(allTaskFields.completed, allParameterComparators.isFalse, null),
      new SearchParameter(allTaskFields.dueDate, allParameterComparators.onBefore,
        new RelativeDate(allRelativeDateNames.relativeDay, 0))
    ];
    todayView.sortParameters = [
      new SortParameter(allTaskFields.dueDate, false, false),
      new SortParameter(allTaskFields.editDate, false, true)
    ];
    allViews.push(todayView);

    // Shows all completed tasks, sorted by completion date descending (latest first).
    var completedView = new View("Completed");
    completedView.searchParameters = [
      new SearchParameter(allTaskFields.completed, allParameterComparators.isTrue, null)
    ];
    completedView.sortParameters = [
      new SortParameter(allTaskFields.completionDate, true, true)
    ];
    allViews.push(completedView);

    save();
  };

  // Load the views from a JSON string.
  // Return true on success, false on failure.
  var loadJsonViews = function(json) {
    var loadedViews = angular.fromJson(json);

    if (!loadedViews) {
      return false;
    }

    loadViews(loadedViews);
    return true;
  };

  // Load the views from a plain JS object.
  var loadViews = function(views) {
    // Reconstruct each view object.
    for (var i in views) {
      if (!views.hasOwnProperty(i)) {
        continue;
      }

      var loadedView = views[i];
      var newView = new View("");
      newView.copyFrom(loadedView);
      allViews.push(newView);
    }

    save();
  };

  // The array holding all the views.
  var allViews = [];

  // First, try to load the views from local storage.
  var success = loadJsonViews(angular.fromJson(window.localStorage['views']));

  if (!success) {
    // Load the default views if that fails.
    resetToDefault();
  }

  return {
    addNewView: addNewView,
    allViews: allViews,
    getId: getIndex,
    save: save,
    reset: resetToDefault,

    // Get the view for a given index.
    getView: function (viewId) {
      return allViews[viewId];
    },

    // Remove the given view.
    removeView: function (view) {
      var i = getIndex(view);

      if (i !== null) {
        allViews.splice(i, 1);
      }
    },

    // Add a view.
    addView: function (view) {
      allViews.push(view);
    },

    // Output all views to console as JSON.
    outputViews: function () {
      console.log(angular.toJson(allViews));
    },

    // Load a prepared set of views for demonstrating this task manager.
    loadDemoData: function () {
      allViews.splice(0, allViews.length);
      loadViews(demoViews);
    }
  };
});
