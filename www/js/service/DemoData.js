// Kind of a kludge... this file just contains huge objects containing the tasks and views for the demo.

// Method that adds all properties from a source object to a target object.
var addAll = function (target, source) {
  for (var key in source) {
    if (!source.hasOwnProperty(key)) {
      continue;
    }
    target[key] = source[key];
  }
};

// Initialize the views array.
var demoViews = [{
  "title": "Today",
  "searchParameters": [{
    "field": {
      "name": "completed",
      "prettyName": "completed",
      "type": "boolean",
      "canBeNull": false
    }, "comparator": {"name": "isFalse", "allowedFieldType": "boolean", "allowedValueTypes": []}, "value": null
  }, {
    "field": {"name": "dueDate", "prettyName": "due date", "type": "Date", "canBeNull": true},
    "comparator": {"name": "onBefore", "allowedFieldType": "Date", "allowedValueTypes": ["Date", "RelativeDate"]},
    "value": {"name": "relativeDay", "offset": 0}
  }],
  "sortParameters": [{
    "field": {"name": "dueDate", "prettyName": "due date", "type": "Date", "canBeNull": true},
    "nullFirst": false,
    "descending": false
  }, {
    "field": {"name": "editDate", "prettyName": "last edit date", "type": "Date", "canBeNull": false},
    "nullFirst": false,
    "descending": true
  }]
}, {
  "title": "This Week",
  "searchParameters": [{
    "field": {"name": "dueDate", "prettyName": "due date", "type": "Date", "canBeNull": true},
    "comparator": {"name": "onAfter", "allowedFieldType": "Date", "allowedValueTypes": ["Date", "RelativeDate"]},
    "value": {"name": "relativeWeek", "offset": 0}
  }, {
    "field": {"name": "dueDate", "prettyName": "due date", "type": "Date", "canBeNull": true},
    "comparator": {"name": "before", "allowedFieldType": "Date", "allowedValueTypes": ["Date", "RelativeDate"]},
    "value": {"name": "relativeWeek", "offset": 7}
  }, {
    "field": {"name": "tags", "prettyName": "tags", "type": "Array", "canBeNull": false},
    "comparator": {"name": "arrayNotIncludes", "allowedFieldType": "Array", "allowedValueTypes": ["string"]},
    "value": "oneday"
  }, {
    "field": {"name": "completed", "prettyName": "completed", "type": "boolean", "canBeNull": false},
    "comparator": {"name": "isFalse", "allowedFieldType": "boolean", "allowedValueTypes": []},
    "value": null
  }],
  "sortParameters": [{
    "field": {"name": "dueDate", "prettyName": "due date", "type": "Date", "canBeNull": true},
    "nullFirst": false,
    "descending": false
  }, {
    "field": {"name": "editDate", "prettyName": "last edit date", "type": "Date", "canBeNull": false},
    "nullFirst": false,
    "descending": true
  }]
}, {
  "title": "Work",
  "searchParameters": [{
    "field": {"name": "tags", "prettyName": "tags", "type": "Array", "canBeNull": false},
    "comparator": {"name": "arrayIncludes", "allowedFieldType": "Array", "allowedValueTypes": ["string"]},
    "value": "work"
  }, {
    "field": {"name": "tags", "prettyName": "tags", "type": "Array", "canBeNull": false},
    "comparator": {"name": "arrayNotIncludes", "allowedFieldType": "Array", "allowedValueTypes": ["string"]},
    "value": "oneday"
  }],
  "sortParameters": [{
    "field": {"name": "dueDate", "prettyName": "due date", "type": "Date", "canBeNull": true},
    "nullFirst": true,
    "descending": false
  }, {
    "field": {"name": "creationDate", "prettyName": "creation date", "type": "Date", "canBeNull": false},
    "nullFirst": false,
    "descending": false
  }]
}, {
  "title": "All",
  "searchParameters": [],
  "sortParameters": [{
    "field": {
      "name": "creationDate",
      "prettyName": "creation date",
      "type": "Date",
      "canBeNull": false
    }, "nullFirst": false, "descending": false
  }, {
    "field": {"name": "dueDate", "prettyName": "due date", "type": "Date", "canBeNull": true},
    "nullFirst": true,
    "descending": false
  }]
}, {
  "title": "Not Completed",
  "searchParameters": [{
    "field": {
      "name": "completed",
      "prettyName": "completed",
      "type": "boolean",
      "canBeNull": false
    }, "comparator": {"name": "isFalse", "allowedFieldType": "boolean", "allowedValueTypes": []}, "value": null
  }],
  "sortParameters": [{
    "field": {"name": "dueDate", "prettyName": "due date", "type": "Date", "canBeNull": true},
    "nullFirst": true,
    "descending": false
  }, {
    "field": {"name": "creationDate", "prettyName": "creation date", "type": "Date", "canBeNull": false},
    "nullFirst": false,
    "descending": false
  }]
}, {
  "title": "Completed",
  "searchParameters": [{
    "field": {
      "name": "completed",
      "prettyName": "completed",
      "type": "boolean",
      "canBeNull": false
    }, "comparator": {"name": "isTrue", "allowedFieldType": "boolean", "allowedValueTypes": []}, "value": null
  }],
  "sortParameters": [{
    "field": {
      "name": "completionDate",
      "prettyName": "completion date",
      "type": "Date",
      "canBeNull": true
    }, "nullFirst": true, "descending": true
  }]
}];

// Initialize the tasks object.
var demoTasks = {};

// The starting tasks.
addAll(demoTasks, {
  "0": {
    "title": "Populate this task manager with all of your tasks",
    "taskId": 0,
    "completed": true,
    "creationDate": "2015-11-12T19:39:45.007Z",
    "completionDate": "2015-11-12T19:41:55.768Z",
    "editDate": "2015-11-12T19:39:45.007Z",
    "dueDate": null,
    "subtasks": [],
    "tags": []
  },
  "1": {
    "title": "Customize the views to your liking",
    "taskId": 1,
    "completed": true,
    "creationDate": "2015-11-12T19:39:45.007Z",
    "completionDate": "2015-11-12T19:41:56.311Z",
    "editDate": "2015-11-12T19:39:45.007Z",
    "dueDate": null,
    "subtasks": [],
    "tags": []
  }
});

// "Eat breakfast" every day for 90 days from November 1st.
addAll(demoTasks, {
  "2": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": "2015-11-01T07:41:57.644Z",
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-01T05:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "3": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": "2015-11-02T07:41:57.644Z",
    "editDate": "2015-11-02T07:41:57.644Z",
    "dueDate": "2015-11-02T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "4": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": "2015-11-03T07:41:57.644Z",
    "editDate": "2015-11-03T07:41:57.644Z",
    "dueDate": "2015-11-03T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "5": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": "2015-11-04T07:41:57.644Z",
    "editDate": "2015-11-04T07:41:57.644Z",
    "dueDate": "2015-11-04T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "6": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": "2015-11-05T07:41:57.644Z",
    "editDate": "2015-11-05T07:41:57.644Z",
    "dueDate": "2015-11-05T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "7": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": "2015-11-06T07:41:57.644Z",
    "editDate": "2015-11-06T07:41:57.644Z",
    "dueDate": "2015-11-06T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "8": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": "2015-11-07T07:41:57.644Z",
    "editDate": "2015-11-07T07:41:57.644Z",
    "dueDate": "2015-11-07T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "9": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": "2015-11-08T07:41:57.644Z",
    "editDate": "2015-11-08T07:41:57.644Z",
    "dueDate": "2015-11-08T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "10": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": "2015-11-09T07:41:57.644Z",
    "editDate": "2015-11-09T07:41:57.644Z",
    "dueDate": "2015-11-09T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "11": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": "2015-11-10T07:41:57.644Z",
    "editDate": "2015-11-10T07:41:57.644Z",
    "dueDate": "2015-11-10T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "12": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-11T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "13": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-12T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "14": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-13T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "15": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-14T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "16": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-15T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "17": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-16T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "18": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-17T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "19": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-18T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "20": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-19T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "21": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-20T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "22": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-21T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "23": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-22T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "24": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-23T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "25": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-24T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "26": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-25T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "27": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-26T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "28": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-27T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "29": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-28T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "30": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-29T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "31": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-11-30T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "32": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-01T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "33": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-02T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "34": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-03T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "35": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-04T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "36": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-05T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "37": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-06T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "38": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-07T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "39": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-08T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "40": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-09T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "41": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": true,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-10T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "42": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-11T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "43": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-12T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "44": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-13T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "45": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-14T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "46": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-15T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "47": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-16T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "48": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-17T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "49": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-18T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "50": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-19T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "51": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-20T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "52": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-21T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "53": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-22T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "54": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-23T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "55": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-24T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "56": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-25T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "57": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-26T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "58": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-27T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "59": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-28T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "60": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-29T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "61": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-30T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "62": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2015-12-31T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "63": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-01T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "64": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-02T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "65": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-03T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "66": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-04T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "67": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-05T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "68": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-06T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "69": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-07T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "70": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-08T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "71": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-09T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "72": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-10T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "73": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-11T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "74": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-12T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "75": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-13T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "76": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-14T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "77": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-15T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "78": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-16T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "79": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-17T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "80": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-18T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "81": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-19T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "82": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-20T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "83": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-21T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "84": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-22T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "85": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-23T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "86": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-24T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "87": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-25T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "88": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-26T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "89": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-27T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "90": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-28T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  },
  "91": {
    "title": "Eat Breakfast",
    "taskId": 2,
    "completed": false,
    "creationDate": "2015-11-01T06:41:57.644Z",
    "completionDate": null,
    "editDate": "2015-11-01T07:41:57.644Z",
    "dueDate": "2016-01-29T06:00:00.000Z",
    "subtasks": [{"title": "Make Coffee", "completed": false}, {
      "title": "Make Toast",
      "completed": false
    }, {"title": "Eat", "completed": false}, {"title": "Wash Dishes", "completed": false}],
    "tags": ["oneday", "personal"]
  }
});

// Go running every monday for 90 days.
addAll(demoTasks, {
  "1000": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": true,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": "2015-11-02T21:13:24.427Z",
    "editDate": "2015-11-02T21:13:26.092Z",
    "dueDate": "2015-11-02T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "1007": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": true,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": "2015-11-09T21:13:24.427Z",
    "editDate": "2015-11-09T21:13:26.092Z",
    "dueDate": "2015-11-09T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "1014": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": true,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-02T21:13:26.092Z",
    "dueDate": "2015-11-16T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "1021": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": true,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-02T21:13:26.092Z",
    "dueDate": "2015-11-23T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "1028": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": true,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-02T21:13:26.092Z",
    "dueDate": "2015-11-30T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "1035": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": true,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-02T21:13:26.092Z",
    "dueDate": "2015-12-07T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "1042": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": false,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-02T21:13:26.092Z",
    "dueDate": "2015-12-14T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "1049": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": false,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-02T21:13:26.092Z",
    "dueDate": "2015-12-21T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "1056": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": false,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-02T21:13:26.092Z",
    "dueDate": "2015-12-28T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "1063": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": false,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-02T21:13:26.092Z",
    "dueDate": "2016-01-04T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "1070": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": false,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-02T21:13:26.092Z",
    "dueDate": "2016-01-11T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "1077": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": false,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-02T21:13:26.092Z",
    "dueDate": "2016-01-18T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "1084": {
    "title": "Go Running",
    "taskId": 1000,
    "completed": false,
    "creationDate": "2015-11-01T21:12:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-02T21:13:26.092Z",
    "dueDate": "2016-01-25T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  }
});

// Do laundry (lights) every Tuesday for 90 days.
addAll(demoTasks, {
  "2000": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": true,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": "2015-11-03T21:15:24.427Z",
    "editDate": "2015-11-03T21:15:26.092Z",
    "dueDate": "2015-11-03T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "2007": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": true,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": "2015-11-10T21:15:24.427Z",
    "editDate": "2015-11-10T21:15:26.092Z",
    "dueDate": "2015-11-10T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "2014": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": true,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-03T21:15:26.092Z",
    "dueDate": "2015-11-17T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "2021": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": true,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-03T21:15:26.092Z",
    "dueDate": "2015-11-24T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "2028": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": true,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-03T21:15:26.092Z",
    "dueDate": "2015-12-01T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "2035": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": true,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-03T21:15:26.092Z",
    "dueDate": "2015-12-08T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "2042": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": false,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-03T21:15:26.092Z",
    "dueDate": "2015-12-15T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "2049": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": false,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-03T21:15:26.092Z",
    "dueDate": "2015-12-22T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "2056": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": false,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-03T21:15:26.092Z",
    "dueDate": "2015-12-29T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "2063": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": false,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-03T21:15:26.092Z",
    "dueDate": "2016-01-05T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "2070": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": false,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-03T21:15:26.092Z",
    "dueDate": "2016-01-12T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "2077": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": false,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-03T21:15:26.092Z",
    "dueDate": "2016-01-19T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "2084": {
    "title": "Do Laundry (lights)",
    "taskId": 2000,
    "completed": false,
    "creationDate": "2015-11-01T21:15:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-03T21:15:26.092Z",
    "dueDate": "2016-01-26T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  }
});

// Lift weights every Wednesday.
addAll(demoTasks, {
  "3000": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": true,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": "2015-11-04T21:17:24.427Z",
    "editDate": "2015-11-04T21:17:26.092Z",
    "dueDate": "2015-11-04T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "3007": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": true,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-11T21:17:26.092Z",
    "dueDate": "2015-11-11T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "3014": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": true,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-04T21:17:26.092Z",
    "dueDate": "2015-11-18T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "3021": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": true,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-04T21:17:26.092Z",
    "dueDate": "2015-11-25T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "3028": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": true,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-04T21:17:26.092Z",
    "dueDate": "2015-12-02T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "3035": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": true,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-04T21:17:26.092Z",
    "dueDate": "2015-12-09T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "3042": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": false,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-04T21:17:26.092Z",
    "dueDate": "2015-12-16T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "3049": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": false,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-04T21:17:26.092Z",
    "dueDate": "2015-12-23T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "3056": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": false,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-04T21:17:26.092Z",
    "dueDate": "2015-12-30T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "3063": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": false,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-04T21:17:26.092Z",
    "dueDate": "2016-01-06T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "3070": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": false,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-04T21:17:26.092Z",
    "dueDate": "2016-01-13T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "3077": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": false,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-04T21:17:26.092Z",
    "dueDate": "2016-01-20T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "3084": {
    "title": "Lift Weights",
    "taskId": 3000,
    "completed": false,
    "creationDate": "2015-11-01T21:17:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-04T21:17:26.092Z",
    "dueDate": "2016-01-27T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  }
});

// Do laundry (darks) every Thursday.
addAll(demoTasks, {
  "4000": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": true,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": "2015-11-05T21:19:24.427Z",
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2015-11-05T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "4007": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": true,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2015-11-12T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "4014": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": true,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2015-11-19T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "4021": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": true,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2015-11-26T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "4028": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": true,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2015-12-03T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "4035": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": true,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2015-12-10T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "4042": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": false,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2015-12-17T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "4049": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": false,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2015-12-24T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "4056": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": false,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2015-12-31T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "4063": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": false,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2016-01-07T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "4070": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": false,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2016-01-14T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "4077": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": false,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2016-01-21T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "4084": {
    "title": "Do Laundry (darks)",
    "taskId": 4000,
    "completed": false,
    "creationDate": "2015-11-01T21:19:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-05T21:19:26.092Z",
    "dueDate": "2016-01-28T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  }
});

// Pushups every Friday.
addAll(demoTasks, {
  "5000": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": true,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": "2015-11-06T21:23:24.427Z",
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2015-11-06T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "5007": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": true,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2015-11-13T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "5014": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": true,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2015-11-20T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "5021": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": true,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2015-11-27T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "5028": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": true,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2015-12-04T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "5035": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": false,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2015-12-11T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "5042": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": false,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2015-12-18T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "5049": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": false,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2015-12-25T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "5056": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": false,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2016-01-01T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "5063": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": false,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2016-01-08T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "5070": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": false,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2016-01-15T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "5077": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": false,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2016-01-22T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "5084": {
    "title": "Do Pushups",
    "taskId": 5000,
    "completed": false,
    "creationDate": "2015-11-01T21:23:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-06T21:23:26.092Z",
    "dueDate": "2016-01-29T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  }
});

// Grocery store every Saturday.
addAll(demoTasks, {
  "6000": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": true,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": "2015-11-07T21:25:24.427Z",
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2015-11-07T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "6007": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": true,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2015-11-14T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "6014": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": true,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2015-11-21T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "6021": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": true,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2015-11-28T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "6028": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": true,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2015-12-05T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "6035": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": false,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2015-12-12T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "6042": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": false,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2015-12-19T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "6049": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": false,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2015-12-26T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "6056": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": false,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2016-01-02T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "6063": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": false,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2016-01-09T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "6070": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": false,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2016-01-16T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "6077": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": false,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2016-01-23T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "6084": {
    "title": "Go to Grocery Store",
    "taskId": 6000,
    "completed": false,
    "creationDate": "2015-11-01T21:25:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-07T21:25:26.092Z",
    "dueDate": "2016-01-30T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  }
});

// Laundry (colors) every Sunday.
addAll(demoTasks, {
  "7000": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": true,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": "2015-11-01T21:29:24.427Z",
    "editDate": "2015-11-01T21:29:26.092Z",
    "dueDate": "2015-11-01T05:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "7007": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": true,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": "2015-11-08T21:29:24.427Z",
    "editDate": "2015-11-08T21:29:26.092Z",
    "dueDate": "2015-11-08T06:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "7014": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": true,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-01T21:29:26.092Z",
    "dueDate": "2015-11-15T06:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "7021": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": true,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-01T21:29:26.092Z",
    "dueDate": "2015-11-22T06:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "7028": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": true,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-01T21:29:26.092Z",
    "dueDate": "2015-11-29T06:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "7035": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": true,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-01T21:29:26.092Z",
    "dueDate": "2015-12-06T06:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "7042": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": false,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-01T21:29:26.092Z",
    "dueDate": "2015-12-13T06:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "7049": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": false,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-01T21:29:26.092Z",
    "dueDate": "2015-12-20T06:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "7056": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": false,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-01T21:29:26.092Z",
    "dueDate": "2015-12-27T06:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "7063": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": false,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-01T21:29:26.092Z",
    "dueDate": "2016-01-03T06:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "7070": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": false,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-01T21:29:26.092Z",
    "dueDate": "2016-01-10T06:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "7077": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": false,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-01T21:29:26.092Z",
    "dueDate": "2016-01-17T06:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  },
  "7084": {
    "title": "Do laundry (colors)",
    "taskId": 7000,
    "completed": false,
    "creationDate": "2015-11-01T21:29:25.671Z",
    "completionDate": null,
    "editDate": "2015-11-01T21:29:26.092Z",
    "dueDate": "2016-01-24T06:00:00.000Z",
    "subtasks": [],
    "tags": ["oneday", "personal"]
  }
});

// Add tasks related to "work" tag.
addAll(demoTasks, {
  "8183": {
    "title": "Brainstorm project ideas",
    "taskId": 8183,
    "completed": true,
    "creationDate": "2015-11-12T21:44:36.459Z",
    "completionDate": "2015-11-12T21:45:00.829Z",
    "editDate": "2015-12-08T19:13:54.231Z",
    "dueDate": "2015-11-30T05:00:00.000Z",
    "subtasks": [],
    "tags": ["work"]
  },
  "8184": {
    "title": "Do Overall Project Design",
    "taskId": 8184,
    "completed": true,
    "creationDate": "2015-11-12T21:45:11.194Z",
    "completionDate": "2015-11-12T21:45:35.214Z",
    "editDate": "2015-12-08T19:14:18.358Z",
    "dueDate": "2015-12-01T05:00:00.000Z",
    "subtasks": [],
    "tags": ["work"]
  },
  "8185": {
    "title": "Design User Interface",
    "taskId": 8185,
    "completed": true,
    "creationDate": "2015-11-12T21:45:46.315Z",
    "completionDate": "2015-11-12T21:46:02.830Z",
    "editDate": "2015-12-08T19:14:25.207Z",
    "dueDate": "2015-12-02T05:00:00.000Z",
    "subtasks": [],
    "tags": ["work"]
  },
  "8186": {
    "title": "Design Core Modules",
    "taskId": 8186,
    "completed": true,
    "creationDate": "2015-11-12T21:46:06.970Z",
    "completionDate": "2015-11-12T21:46:33.926Z",
    "editDate": "2015-11-12T21:48:53.574Z",
    "dueDate": "2015-11-05T05:00:00.000Z",
    "subtasks": [],
    "tags": []
  },
  "8187": {
    "title": "Write Unit Tests for Core Modules",
    "taskId": 8187,
    "completed": true,
    "creationDate": "2015-11-12T21:46:36.627Z",
    "completionDate": "2015-11-12T21:47:19.006Z",
    "editDate": "2015-12-08T19:14:32.118Z",
    "dueDate": "2015-12-04T05:00:00.000Z",
    "subtasks": [],
    "tags": ["work"]
  },
  "8188": {
    "title": "Implement Core Modules",
    "taskId": 8188,
    "completed": true,
    "creationDate": "2015-11-12T21:47:23.803Z",
    "completionDate": "2015-11-12T21:48:28.799Z",
    "editDate": "2015-12-08T19:14:41.766Z",
    "dueDate": "2015-12-08T05:00:00.000Z",
    "subtasks": [],
    "tags": ["work"]
  },
  "8189": {
    "title": "Debug Core Modules",
    "taskId": 8189,
    "completed": true,
    "creationDate": "2015-11-12T21:48:59.171Z",
    "completionDate": null,
    "editDate": "2015-12-08T19:14:47.183Z",
    "dueDate": "2015-12-09T05:00:00.000Z",
    "subtasks": [],
    "tags": ["work"]
  },
  "8190": {
    "title": "Implement User Interface",
    "taskId": 8190,
    "completed": true,
    "creationDate": "2015-11-12T21:51:25.873Z",
    "completionDate": null,
    "editDate": "2015-12-08T19:14:53.911Z",
    "dueDate": "2015-12-10T05:00:00.000Z",
    "subtasks": [],
    "tags": ["work"]
  },
  "8191": {
    "title": "Test User Interface",
    "taskId": 8191,
    "completed": false,
    "creationDate": "2015-11-12T21:51:47.731Z",
    "completionDate": null,
    "editDate": "2015-12-08T19:15:03.151Z",
    "dueDate": "2015-12-11T05:00:00.000Z",
    "subtasks": [],
    "tags": ["work"]
  },
  "8192": {
    "title": "Run Code Quality Tools",
    "taskId": 8192,
    "completed": false,
    "creationDate": "2015-11-12T21:52:18.315Z",
    "completionDate": null,
    "editDate": "2015-12-08T19:15:29.896Z",
    "dueDate": "2015-12-15T05:00:00.000Z",
    "subtasks": [],
    "tags": ["work"]
  },
  "8193": {
    "title": "Perform End-To-End Testing",
    "taskId": 8193,
    "completed": false,
    "creationDate": "2015-11-12T21:53:09.229Z",
    "completionDate": null,
    "editDate": "2015-12-08T19:15:48.937Z",
    "dueDate": "2015-12-16T05:00:00.000Z",
    "subtasks": [],
    "tags": ["work"]
  },
  "8194": {
    "title": "Publish Project",
    "taskId": 8194,
    "completed": false,
    "creationDate": "2015-11-12T21:54:46.508Z",
    "completionDate": null,
    "editDate": "2015-12-08T19:15:54.328Z",
    "dueDate": "2015-12-18T05:00:00.000Z",
    "subtasks": [],
    "tags": ["work"]
  }
});
