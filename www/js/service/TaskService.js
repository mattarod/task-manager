var module = angular.module('taskManager.features.service');

// The service that manages tasks.
module.factory('TaskService', function () {
  // The last unique ID assigned to a task.
  var lastId = -1;

  // Get a new unique ID for a new task.
  var getNextId = function () {
    lastId++;
    return lastId;
  };

  // Create a new task with a given title and add it to the allTasks object.
  var addNewTask = function (title) {
    var task = new Task(title, getNextId());
    allTasks[task.taskId] = task;
    return task;
  };

  // Save all tasks to local storage.
  var save = function () {
    window.localStorage['tasks'] = angular.toJson(allTasks);
  };

  // Delete all tasks.
  var deleteAllTasks = function () {
    for (var key in allTasks) {
      if (allTasks.hasOwnProperty(key)) {
        delete allTasks[key];
      }
    }

    lastId = -1;
  };

  // Reset to the default tasks.
  var resetToDefault = function() {
    deleteAllTasks();
    addNewTask("Populate this task manager with all of your tasks");
    addNewTask("Customize the views to your liking");
    save();
  };

  // Load the tasks from a JSON string.
  // Return true on success, false on failure.
  var loadJsonTasks = function (json) {
    var loadedTasks = angular.fromJson(json);

    if (!loadedTasks) {
      return false;
    }

    loadTasks(loadedTasks);
    return true;
  };

  // Load the tasks from a plain JS object.
  var loadTasks = function(tasks) {
    // Reconstruct each task object.
    for (var id in tasks) {
      if (!tasks.hasOwnProperty(id)) {
        continue;
      }

      var loadedTask = tasks[id];
      var newTask = addNewTask("");
      newTask.copyFrom(loadedTask);
      allTasks[newTask.taskId] = newTask;
    }

    save();
  };

  // The data structure holding all the tasks.
  var allTasks = {};

  // First, try to load the tasks from local storage.
  var success = loadJsonTasks(window.localStorage['tasks']);

  if (!success) {
    // Load the default tasks if that fails.
    resetToDefault();
  }

  return {
    addNewTask: addNewTask,
    reset: resetToDefault,
    deleteAllTasks: deleteAllTasks,
    save: save,

    // Get the tasks for a view.
    getTasksForView: function (view) {
      return view.getTaskArray(allTasks);
    },

    // Get the task for an ID.
    getTask: function (taskId) {
      return allTasks[taskId];
    },

    // Remove the task for an ID.
    removeTask: function (taskId) {
      delete allTasks[taskId];
    },

    // Add the given task to the service.
    addTask: function (task) {
      // Assign an ID if there isn't one.
      if (task.taskId === null) {
        task.taskId = getNextId();
      }

      // Insert it into the allTasks object.
      allTasks[task.taskId] = task;
    },

    // Output all tasks to console as JSON.
    outputTasks: function () {
      console.log(angular.toJson(allTasks));
    },

    // Load a prepared set of tasks for demonstrating this task manager.
    loadDemoData: function () {
      deleteAllTasks();
      loadTasks(demoTasks);
    }
  }
})
;
