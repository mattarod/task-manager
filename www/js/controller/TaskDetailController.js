var module = angular.module('taskManager.features.controller');

// The controller for the Task page.
module.controller('TaskDetailController', function ($scope, $state, $stateParams, $ionicPlatform, TaskService) {
  // Create an object in the $scope for interfacing with the HTML page.
  var taskDetail = {};
  $scope.taskDetail = taskDetail;

  // Get the task for the given ID.
  var task = TaskService.getTask(parseInt($stateParams.taskId));

  // If the task doesn't exist, create it.
  if (!task) {
    task = new Task("", null);
  }

  // If the task name is "New Task", set it to the empty string.
  if(task.title === "New Task") {
    task.title = "";
  }

  taskDetail.task = task;

  // Convert the tag array to a string and write it to the page.
  taskDetail.tagsString = task.tags.join(" ");

  // Go back to the main page.
  taskDetail.back = function () {
    taskMaintenance();
    TaskService.save();
    $state.go("main", {}, {reload: true});
    $scope.main.refreshView();
  };

  // Perform maintenance on the task before leaving the page.
  var taskMaintenance = function() {
    task.editDate = new Date();

    // Split the tag string on any block of whitespace.
    // Sort the result alphabetically and write it to the task.
    var tags = taskDetail.tagsString.split(/\s+/);
    tags.sort();

    // Remove all whitespace-only tags.
    for(var i = 0; i < tags.length;) {
      var tag = tags[i];

      if(tag.trim() === "") {
        tags.splice(i, 1);
      } else {
        i++;
      }
    }
    task.tags = tags;

    // Delete empty subtasks.
    for (i = 0; i < task.subtasks.length;) {
      var subtask = task.subtasks[i];

      // Trim whitespace from the subtask title.
      subtask.title = subtask.title.trim();

      // If the subtask title is empty, delete it.
      if (subtask.title === "") {
        task.subtasks.splice(i, 1);
      }
      // Otherwise, increment the index to the next subtask in the array.
      else {
        i++;
      }
    }

    // Trim whitespace from the task title.
    task.title = task.title.trim();

    if(task.title === "") {
      // If the task name, subtask list, and tags field are empty, delete this task.
      if(task.subtasks.length === 0 && task.tags.length === 0) {
        TaskService.removeTask(task.taskId);
        return;
      }

      // If the task name is empty but there are either subtasks or tags, assign a generic name.
      task.title = "New Task";
    }

    // If creating a new task, add it to the service.
    if (task.taskId === null) {
      TaskService.addTask(task);
    }
  };

  // Get the title of the page.
  taskDetail.getTitle = function () {
    if (task.title.trim() === "") {
      return "New Task";
    } else {
      return task.title;
    }
  };

  // Toggle whether the task is completed.
  taskDetail.toggleCompleted = function () {
    task.toggleCompleted();
  };

  // Add a new subtask.
  taskDetail.addSubtask = function () {
    task.subtasks.push({});
  };

  // Toggle a subtask.
  taskDetail.toggleSubtask = function (subtask) {
    subtask.completed = !subtask.completed;
  };

  // Delete a given subtask.
  taskDetail.deleteSubtask = function (subtask) {
    for (var i in task.subtasks) {
      if(!task.subtasks.hasOwnProperty(i)) {
        continue;
      }

      if (subtask == task.subtasks[i]) {
        task.subtasks.splice(i, 1);
        return;
      }
    }

    console.log("Warning: tried to delete subtask " + angular.toJson(subtask) + " but it apparently doesn't exist.");
  };

  // Properly handle it when the platform's back button is pressed.
  var deregisterBackButtonAction = $ionicPlatform.registerBackButtonAction (
    function () {
      if($state.is('taskDetail')) {
        taskDetail.back();
      }
    }, 101
  );

  $scope.$on('$destroy', deregisterBackButtonAction);
});
