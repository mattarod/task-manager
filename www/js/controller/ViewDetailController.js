var module = angular.module('taskManager.features.controller');

// The controller for the View page.
module.controller('ViewDetailController', function ($scope, $state, $stateParams, $ionicModal,
                                                    $ionicPlatform, ViewService) {
  // Vars global to the ViewDetail page used by the parameter modals.
  var currentParameter;
  var addingNewParameter;

  // Create an object in the $scope for interfacing with the HTML page.
  var viewDetail = {};
  $scope.viewDetail = viewDetail;

  // Get the view for the given ID.
  var view = ViewService.getView(parseInt($stateParams.viewId));

  if(!view) {
    view = new View("");
  }

  viewDetail.view = view;

  // Create and load the search param modal.
  $ionicModal.fromTemplateUrl('search-param-modal.html', function(modal) {
    viewDetail.searchParamModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });

  // Create and load the sort param modal.
  $ionicModal.fromTemplateUrl('sort-param-modal.html', function(modal) {
    viewDetail.sortParamModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });

  // Go back to the views page.
  viewDetail.back = function () {
    viewMaintenance();
    ViewService.save();
    $state.go("views", {}, {reload: true});
  };

  // Perform maintenance on the view before leaving the page.
  var viewMaintenance = function() {
    // Trim whitespace from the view name.
    view.title = view.title.trim();

    if(view.title === "") {
      // If view name and parameters are empty, delete this view.
      if(view.searchParameters.length === 0 && view.sortParameters.length === 0) {
        ViewService.removeView(view);
        return;
      }

      // If the view title is empty but there are parameters, assign a default name.
      view.title = "New View";
    }

    // If this is a new view, add it to the service.
    if(ViewService.getId(view) === null) {
      ViewService.addView(view);
    }
  };

  // Edit a search parameter.
  viewDetail.editSearchParameter = function(parameter) {
    addingNewParameter = false;
    viewDetail.errorMessage = "";

    if(parameter === null) {
      parameter = new SearchParameter(allTaskFields.blank, allParameterComparators.blank, null);
      addingNewParameter = true;
    }

    viewDetail.field = parameter.field.name;
    viewDetail.comparator = parameter.comparator.name;
    viewDetail.stringValue = parameter.getStringValue();
    viewDetail.dateValue = parameter.getDateValue();
    viewDetail.dateType = parameter.getDateType();

    currentParameter = parameter;

    viewDetail.searchParamModal.show();
  };

  // Edit a sort parameter.
  viewDetail.editSortParameter = function(parameter) {
    addingNewParameter = false;
    viewDetail.errorMessage = "";

    if(parameter === null) {
      parameter = new SortParameter(allTaskFields.blank, false);
      addingNewParameter = true;
    }

    viewDetail.field = parameter.field.name;
    viewDetail.ascending = !parameter.descending;
    viewDetail.nullFirst = parameter.nullFirst;

    currentParameter = parameter;

    viewDetail.sortParamModal.show();
  };

  // Overwrite the contents of the current search parameter with the contents of the modal.
  viewDetail.commitSearchParameter = function() {
    // Create a copy of the current parameter for validation.
    var validationParameter = new SearchParameter(currentParameter.field,
      currentParameter.comparator, currentParameter.value);

    // Write the data from the modal to the validation parameter.
    validationParameter.createFromViewDetail(viewDetail);

    // If the data in the modal is not valid, show an error message and return without closing the modal.
    if(!validationParameter.isValid()) {
      viewDetail.errorMessage = "Please set all of the fields.";
      return;
    }

    // If everything checks out, write the data from the modal to the parameter.
    currentParameter.field = validationParameter.field;
    currentParameter.comparator = validationParameter.comparator;
    currentParameter.value = validationParameter.value;

    // If this is a new parameter, add it to the view.
    if(addingNewParameter) {
      view.searchParameters.push(currentParameter);
    }

    // Hide the modal.
    viewDetail.searchParamModal.hide();

    // Reload the page.
    $state.go("viewDetail", {viewId: $stateParams.viewId}, {reload: true});
  };

  // Overwrite the contents of the current sort parameter with the contents of the modal.
  viewDetail.commitSortParameter = function() {
    // Ensure the field is set.
    if(!allTaskFields.hasOwnProperty(viewDetail.field)) {
      viewDetail.errorMessage = "Please set all of the fields.";
      return;
    }

    // Write the data from the modal to the parameter.
    currentParameter.field = allTaskFields[viewDetail.field];
    currentParameter.nullFirst = viewDetail.nullFirst;
    currentParameter.descending = !viewDetail.ascending;

    // If this is a new parameter, add it to the view.
    if(addingNewParameter) {
      view.sortParameters.push(currentParameter);
    }

    // Hide the modal.
    viewDetail.sortParamModal.hide();

    // Reload the page.
    $state.go("viewDetail", {viewId: $stateParams.viewId}, {reload: true});
  };

  // Close the search parameter modal.
  viewDetail.closeSearchParameterModal = function() {
    viewDetail.searchParamModal.hide();
  };

  // Close the sort parameter modal.
  viewDetail.closeSortParameterModal = function() {
    viewDetail.sortParamModal.hide();
  };

  // Delete a selected search parameter.
  viewDetail.deleteSearchParameter = function(parameterToDelete) {
    for(var i in view.searchParameters) {
      if(!view.searchParameters.hasOwnProperty(i)) {
        continue;
      }
      var parameter = view.searchParameters[i];
      if(parameter === parameterToDelete) {
        view.searchParameters.splice(i,1);
      }
    }
  };

  // Delete a selected sort parameter.
  viewDetail.deleteSortParameter = function(parameterToDelete) {
    for(var i in view.sortParameters) {
      if(!view.sortParameters.hasOwnProperty(i)) {
        continue;
      }
      var parameter = view.sortParameters[i];
      if(parameter === parameterToDelete) {
        view.sortParameters.splice(i,1);
      }
    }
  };

  // Determine the type of the current field.
  viewDetail.fieldType = function() {
    var taskField = allTaskFields[viewDetail.field];

    if(!taskField) {
      return null;
    }

    var comparator = allParameterComparators[viewDetail.comparator];

    // If the current comparator exists, and is not applicable to the current field, set it to null.
    if(comparator && comparator.allowedFieldType !== taskField.type) {
      viewDetail.comparator = null;
    }

    return taskField.type;
  };

  // A function that determines if the current field can possibly be null.
  viewDetail.fieldCanBeNull = function() {
    var taskField = allTaskFields[viewDetail.field];

    if(!taskField) {
      return false;
    }

    return taskField.canBeNull;
  };

  // Determine the type of the value for the current field and comparator.
  viewDetail.valueType = function() {
    // First, make sure the field is set.
    // If the field isn't set, we're not ready to show the value selector yet.
    var taskField = allTaskFields[viewDetail.field];

    if(!taskField) {
      return null;
    }

    // Second, make sure the comparator is set, for the same reason.
    var comparator = allParameterComparators[viewDetail.comparator];

    if(!comparator) {
      return null;
    }

    // Third, make sure the comparator is compatible with the field.
    // Otherwise, clear it, and return null.
    if(comparator.allowedFieldType !== taskField.type) {
      viewDetail.comparator = null;
      return null;
    }


    // Finally, get and return the comparator's canonical type.
    return comparator.canonicalValueType();
  };

  // Properly handle it when the platform's back button is pressed.
  var deregisterBackButtonAction = $ionicPlatform.registerBackButtonAction (
    function () {
      if($state.is('viewDetail')) {
        viewDetail.back();
      }
    }, 101
  );

  $scope.$on('$destroy', deregisterBackButtonAction);
});
