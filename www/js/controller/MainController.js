var module = angular.module('taskManager.features.controller');

// The controller for the Main page.
module.controller('MainController', function ($scope, $state, TaskService, ViewService,
                                              $ionicSideMenuDelegate, $ionicActionSheet) {

  // Create an object in the $scope for interfacing with the HTML page.
  var main = {};
  $scope.main = main;

  // Toggle whether a task is complete.
  // Mark task successful if setting complete to true.
  main.toggleCompleted = function (task) {
    task.toggleCompleted();
    TaskService.save();
    main.refreshView();
  };

  // Show and hide left menu
  main.toggleLeftMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };

  // Set the desired view
  main.setView = function (view) {
    main.currentView = view;
    main.refreshView();
  };

  // Load the Task page for the given task ID.
  main.editTask = function (taskId) {
    $state.go("taskDetail", {taskId: taskId}, {reload: true});
  };

  // Go to view customization page.
  main.customizeViews = function () {
    $state.go("views", {reload: true});
  };

  // Refresh the view.
  main.refreshView = function () {
    main.currentViewTasks = TaskService.getTasksForView(main.currentView);
  };

  // Test if a task is overdue.
  main.isOverdue = function (task) {
    if (task.completed || !task.hasOwnProperty("dueDate")) {
      return false;
    }

    var today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    today.setMilliseconds(0);
    return task.dueDate < today;
  };

  // Open the debug menu.
  main.openDebugMenu = function () {
    // Show the action sheet
    $ionicActionSheet.show({
      buttons: [
        {text: 'Reset tasks'},
        {text: 'Delete all tasks'},
        {text: 'Reset views'},
        {text: 'Load demo data'},
        {text: 'Dump JSON'}
      ],
      titleText: 'Debug Menu',
      cancelText: 'Close',
      cancel: function () {
      },
      buttonClicked: function (index) {
        switch (index) {
          case 0:
            main.resetTasks();
            break;
          case 1:
            main.deleteAllTasks();
            break;
          case 2:
            main.resetViews();
            break;
          case 3:
            main.loadDemoData();
            break;
          case 4:
            main.outputJson();
            break;
        }
      }
    });
  };

  // Debug option: print tasks and views to console as JSON.
  main.loadDemoData = function () {
    TaskService.loadDemoData();
    ViewService.loadDemoData();
    main.refreshView();
  };


  // Debug option: print tasks and views to console as JSON.
  main.outputJson = function () {
    TaskService.outputTasks();
    ViewService.outputViews();
  };

  // Debug option: reset to default tasks.
  main.resetTasks = function () {
    TaskService.reset();

    // Reload the page.
    main.refreshView();
  };

  // Debug option: reset to default tasks.
  main.deleteAllTasks = function () {
    TaskService.deleteAllTasks();

    // Reload the page.
    main.refreshView();
  };

  // Debug option: reset to default views.
  main.resetViews = function () {
    ViewService.reset();

    main.setView(main.allViews[0]);

    // Reload the page.
    main.refreshView();
  };

  // Initialize views
  main.allViews = ViewService.allViews;
  main.setView(main.allViews[0]);
});
