var module = angular.module('taskManager.features.controller');

// The controller for the Views page.
module.controller('ViewsController', function ($scope, $state, $ionicPlatform, ViewService) {
  // Create an object in the $scope for interfacing with the HTML page.
  var views = {};
  $scope.views = views;

  // Go back to the main page.
  views.back = function () {
    $state.go("main", {}, {reload: true});
    $scope.main.refreshView();
  };

  // Go to the view detail page.
  views.editView = function (view) {
    var viewId = ViewService.getId(view);

    $state.go("viewDetail", {viewId: viewId}, {reload: true});
  };

  // Properly handle it when the platform's back button is pressed.
  var deregisterBackButtonAction = $ionicPlatform.registerBackButtonAction (
    function () {
      if($state.is('views')) {
        views.back();
      }
    }, 101
  );

  $scope.$on('$destroy', deregisterBackButtonAction);

  views.allViews = ViewService.allViews;
});
